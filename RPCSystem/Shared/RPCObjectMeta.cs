﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Bitz.Modules.Core.Networking.RPCSystem.Shared
{
    class RPCObjectMeta
    {
        internal struct RPCObjectPropertyMeta
        {
            internal readonly String ParameterName;
            internal readonly PropertyInfo PropertyInfo;

            internal RPCObjectPropertyMeta(String name, PropertyInfo pInfo)
            {
                ParameterName = name;
                PropertyInfo = pInfo;
            }
        }

        private readonly String _LookupString;

        internal readonly String ObjectName;
        internal readonly Type ObjectType;
        internal readonly List<RPCObjectPropertyMeta> PropertyInfo = new List<RPCObjectPropertyMeta>();

        internal String LookupString => _LookupString;

        internal RPCObjectMeta(Type objectType)
        {
            ObjectType = objectType;
            ObjectName = objectType.Name;

            StringBuilder lookupString = new StringBuilder($"{objectType.Name}");
            foreach (PropertyInfo propInfo in objectType.GetProperties())
            {
                if (propInfo.CustomAttributes.All(a => a.AttributeType != typeof(RPCEnabled))) continue;
                PropertyInfo.Add(new RPCObjectPropertyMeta(propInfo.Name, propInfo));
                lookupString.Append($":{propInfo.Name}/{propInfo.Name}");
            }
            _LookupString = lookupString.ToString();
        }
    }
}
