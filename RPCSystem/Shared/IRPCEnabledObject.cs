﻿using System;

namespace Bitz.Modules.Core.Networking.RPCSystem.Shared
{
    public interface IRPCEnabledObject
    {
        [RPCEnabled]
        Int64 RCPID { get; set; }
        Boolean NetSendStale { get; set; }
    }
}
