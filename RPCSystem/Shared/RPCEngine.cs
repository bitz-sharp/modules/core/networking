﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Bitz.Modules.Core.Foundation;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Logic;
using Bitz.Modules.Core.Networking.RPCSystem.Server;
using Sbatman.Networking.Client;
using Sbatman.Networking.Server;
using Sbatman.Serialize;

namespace Bitz.Modules.Core.Networking.RPCSystem.Shared
{
    public class RPCEngine : BasicObject, IUpdateable
    {
        private enum Packettypes
        {
            EXECUTE_RPC_STATIC = 564,
            EXECUTE_RPC_STATIC_WITH_ARGS,
            EXECUTE_RPC_STATIC_WITH_REPLY,
            REGISTER_RPC_OBJECT_UPDATE,
            PACKET_OBJECT,
            EXECUTE_RPC_OBJECT,
            EXECUTE_RPC_OBJECT_WITH_REPLY,
            EXECUTE_RPC_OBJECT_WITH_ARGS,
            REPLY
        }

        private struct DeferedRequest
        {
            public readonly Int64 ID;
            public readonly Action<Object> Action;

            public DeferedRequest(Int64 id, Action<Object> action)
            {
                ID = id;
                Action = action;
            }
        }

        private readonly List<RPCMethodMeta> _KnownRPCMethods;
        private readonly List<RPCObjectMeta> _KnownRPCTypes;

        private BaseClient _Client;
        private ServerInstance _ServerInstance;
        private IPAddress _TargetIPAddress;
        private UInt16 _TargetPort;
        private Boolean _ConnecitvityStarted;
        private Action<Object> _OnObjectCreated;
        private Int64 _LastRPCObjectID;
        private Int64 _LastRPCDeferedID;

        private readonly Dictionary<Int64, IRPCEnabledObject> _RegisteredObjectList = new Dictionary<Int64, IRPCEnabledObject>();
        private readonly List<DeferedRequest> _DeferedRequests = new List<DeferedRequest>();

        private IGameLogicService _GameLogicService;

        public Action<Object> OnObjectCreated
        {
            set => _OnObjectCreated = value;
        }

        private static RPCEngine _Singleton;

        public static RPCEngine Singleton => _Singleton;

        public override void Dispose()
        {
            _GameLogicService.UnRegisterIUpdateable(this);
            _ServerInstance.Dispose();
            _ServerInstance = null;
            _OnObjectCreated = null;
            _Client?.Disconnect();
            _KnownRPCMethods.Clear();
            _KnownRPCTypes.Clear();
            _Singleton = null;
            base.Dispose();
        }

        private RPCEngine()
        {
            _GameLogicService = Injector.GetSingleton<IGameLogicService>();
            IEnumerable<Type> types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).ToArray();
            _KnownRPCMethods = types.SelectMany(x => x.GetMethods().Where(y => y.CustomAttributes.Any(a => a.AttributeType == typeof(RPCEnabled)))).Select(a => new RPCMethodMeta(a)).ToList();
            _KnownRPCTypes = types.Where(a => a.GetInterfaces().Contains(typeof(IRPCEnabledObject))).Select(a => new RPCObjectMeta(a)).ToList();

            _Singleton = this;
            _GameLogicService.RegisterIUpdateable(this);
        }

        public void RegisterRPCEnabledObject(IRPCEnabledObject theObject)
        {
            if (IsDisposed()) throw new ObjectDisposedException(GetType().FullName);
            if (IsClient) throw new InvalidOperationException("Clients should not attempt to register objects, this should be done by the server through an RPC call and then the object will pass to the client");
            if (theObject.RCPID != 0)
            {
                Boolean alreadyRegistered = _RegisteredObjectList.ContainsKey(theObject.RCPID);
                if (alreadyRegistered)
                {
                    throw new ArgumentException("This object is already regsitered", nameof(theObject));
                }
            }
            theObject.RCPID = ++_LastRPCObjectID;
            theObject.NetSendStale = true;
            _RegisteredObjectList.Add(theObject.RCPID, theObject);
        }

        public void BeginConnectivity()
        {
            if (IsDisposed()) throw new ObjectDisposedException(GetType().FullName);
            if (_ConnecitvityStarted) throw new InvalidOperationException("Connectivity already started");
            _ConnecitvityStarted = true;
            if (_ServerInstance != null)
            {
                _ServerInstance.Init(new IPEndPoint(_TargetIPAddress, _TargetPort), typeof(ConnectedClient));
                _ServerInstance.StartListening();
            }
            else if (_Client != null)
            {
                _Client.Connect(new IPEndPoint(_TargetIPAddress, _TargetPort));
            }
            else
            {
                throw new InvalidOperationException($"{nameof(_ServerInstance)} & {nameof(_Client)} cannot both be null");
            }
        }

        public void Invoke(Action method, ConnectedClient specificClient = null)
        {
            if (IsDisposed()) throw new ObjectDisposedException(GetType().FullName);
            Packet p = new Packet((UInt16)(method.Target != null ? Packettypes.EXECUTE_RPC_OBJECT : Packettypes.EXECUTE_RPC_STATIC));
            p.Add(method.Method.Name);
            if (method.Target != null) p.Add(((IRPCEnabledObject)method.Target).RCPID);
            if (_ServerInstance != null)
            {
                if (specificClient != null) specificClient.SendPacket(p);
                else _ServerInstance.SendToAll(p);
            }
            else if (_Client != null) _Client.SendPacket(p);
            else
            {
                throw new InvalidOperationException($"{nameof(_ServerInstance)} & {nameof(_Client)} cannot both be null");
            }
        }

        public void InvokeWithResponse<T>(Func<T> method, Action<Object> onRecieve, ConnectedClient specificClient = null)
        {
            if (IsDisposed()) throw new ObjectDisposedException(GetType().FullName);
            Packet p = new Packet((UInt16)(method.Target != null ? Packettypes.EXECUTE_RPC_OBJECT_WITH_REPLY : Packettypes.EXECUTE_RPC_STATIC_WITH_REPLY));
            Int64 requestID = _LastRPCDeferedID++;
            p.Add(method.Method.Name);
            p.Add(requestID);
            _DeferedRequests.Add(new DeferedRequest(requestID, onRecieve));

            if (method.Target != null) p.Add(((IRPCEnabledObject)method.Target).RCPID);
            if (_ServerInstance != null)
            {
                if (specificClient != null) specificClient.SendPacket(p);
                else _ServerInstance.SendToAll(p);
            }
            else if (_Client != null) _Client.SendPacket(p);
            else
            {
                throw new InvalidOperationException($"{nameof(_ServerInstance)} & {nameof(_Client)} cannot both be null");
            }
        }

        public void Invoke<T>(Action<T> method, T arg, ConnectedClient specificClient = null)
        {
            if (IsDisposed()) throw new ObjectDisposedException(GetType().FullName);
            Packet p = new Packet((UInt16)(method.Target != null ? Packettypes.EXECUTE_RPC_OBJECT_WITH_ARGS : Packettypes.EXECUTE_RPC_STATIC_WITH_ARGS));
            p.Add($"{method.Method.Name}:{typeof(T)}");
            if (method.Target != null) p.Add(((IRPCEnabledObject)method.Target).RCPID);
            p.AddObject(typeof(T) == typeof(ClientConnection) ? "" : (Object)arg);

            if (_ServerInstance != null)
            {
                if (specificClient != null) specificClient.SendPacket(p);
                else _ServerInstance.SendToAll(p);
            }
            else if (_Client != null) _Client.SendPacket(p);
            else
            {
                throw new InvalidOperationException($"{nameof(_ServerInstance)} & {nameof(_Client)} cannot both be null");
            }
        }

        public void Invoke<T1, T2>(Action<T1, T2> method, T1 arg, T2 arg2, ConnectedClient specificClient = null)
        {
            if (IsDisposed()) throw new ObjectDisposedException(GetType().FullName);
            Packet p = new Packet((UInt16)(method.Target != null ? Packettypes.EXECUTE_RPC_OBJECT_WITH_ARGS : Packettypes.EXECUTE_RPC_STATIC_WITH_ARGS));
            p.Add($"{method.Method.Name}:{typeof(T1)}:{typeof(T2)}");
            if (method.Target != null) p.Add(((IRPCEnabledObject)method.Target).RCPID);
            p.AddObject(typeof(T1) == typeof(ClientConnection) ? "" : (Object)arg);
            p.AddObject(typeof(T2) == typeof(ClientConnection) ? "" : (Object)arg2);

            if (_ServerInstance != null)
            {
                if (specificClient != null) specificClient.SendPacket(p);
                else _ServerInstance.SendToAll(p);
            }
            else if (_Client != null) _Client.SendPacket(p);
            else
            {
                throw new InvalidOperationException($"{nameof(_ServerInstance)} & {nameof(_Client)} cannot both be null");
            }
        }
        public void Invoke<T1, T2, T3>(Action<T1, T2, T3> method, T1 arg, T2 arg2, T3 arg3, ConnectedClient specificClient = null)
        {
            if (IsDisposed()) throw new ObjectDisposedException(GetType().FullName);
            Packet p = new Packet((UInt16)(method.Target != null ? Packettypes.EXECUTE_RPC_OBJECT_WITH_ARGS : Packettypes.EXECUTE_RPC_STATIC_WITH_ARGS));
            p.Add($"{method.Method.Name}:{typeof(T1)}:{typeof(T2)}:{typeof(T3)}");
            if (method.Target != null) p.Add(((IRPCEnabledObject)method.Target).RCPID);
            p.AddObject(typeof(T1) == typeof(ClientConnection) ? "" : (Object)arg);
            p.AddObject(typeof(T2) == typeof(ClientConnection) ? "" : (Object)arg2);
            p.AddObject(typeof(T3) == typeof(ClientConnection) ? "" : (Object)arg3);

            if (_ServerInstance != null)
            {
                if (specificClient != null) specificClient.SendPacket(p);
                else _ServerInstance.SendToAll(p);
            }
            else if (_Client != null) _Client.SendPacket(p);
            else
            {
                throw new InvalidOperationException($"{nameof(_ServerInstance)} & {nameof(_Client)} cannot both be null");
            }
        }

        public Boolean TryUpdate(TimeSpan currentInstanceTime)
        {
            if (IsDisposed()) throw new ObjectDisposedException(GetType().FullName);
            if (_Client != null)
            {
                foreach (Packet packet in _Client.GetPacketsToProcess())
                {
                    ProcessPacket(packet, null);
                }
            }
            if (_ServerInstance != null)
            {
                Int32 count = 0;
                Packet objectUpdatePacket = new Packet((UInt16)Packettypes.REGISTER_RPC_OBJECT_UPDATE);

                foreach (IRPCEnabledObject rpcObj in _RegisteredObjectList.Values)
                {
                    if (!rpcObj.NetSendStale) continue;
                    Packet packet = ObjectToPacket(rpcObj);
                    rpcObj.NetSendStale = false;
                    objectUpdatePacket.Add(packet);
                    count++;
                }
                if (count != 0)
                {
                    _ServerInstance.SendToAll(objectUpdatePacket);
                }
            }
            return true;
        }

        private Packet ObjectToPacket(IRPCEnabledObject o)
        {
            Packet p = new Packet((UInt16)Packettypes.PACKET_OBJECT);
            RPCObjectMeta rom = _KnownRPCTypes.FirstOrDefault(a => a.ObjectType == o.GetType());
            if (rom == null) throw new ArgumentException($"Object type {o.GetType().Name} not known, please dervive {nameof(IRPCEnabledObject)}");
            p.Add(o.RCPID);
            p.Add(rom.LookupString);
            foreach (RPCObjectMeta.RPCObjectPropertyMeta rpcObjectPropertyMeta in rom.PropertyInfo)
            {
                if (rpcObjectPropertyMeta.PropertyInfo.CustomAttributes.Any(a => a.AttributeType == typeof(RPCEnabled)))
                {
                    Object val = rpcObjectPropertyMeta.PropertyInfo.GetValue(o);
                    if (val == null) continue;
                    p.AddObject(val);
                }
            }
            return p;
        }

        internal void ProcessPacket(Packet p, ClientConnection source)
        {
            if (IsDisposed()) throw new ObjectDisposedException(GetType().FullName);
            switch (p.Type)
            {
                case (UInt16)Packettypes.REPLY:
                    {
                        Object[] data = p.GetObjects();
                        DeferedRequest deferedRequest = _DeferedRequests.FirstOrDefault(a => a.ID.Equals((Int64)data[0]));
                        if (deferedRequest.Action != null)
                        {
                            deferedRequest.Action.Invoke(data[1]);
                        }
                    }
                    break;
                case (UInt16)Packettypes.EXECUTE_RPC_STATIC:
                    {
                        Object[] data = p.GetObjects();
                        String methodName = (String)data[0];
                        RPCMethodMeta method = _KnownRPCMethods.FirstOrDefault(a => a.MethodName.Equals(methodName));
                        method?.MethodInfo.Invoke(null, new Object[] { });
                    }
                    break;
                case (UInt16)Packettypes.EXECUTE_RPC_STATIC_WITH_REPLY:
                    {
                        Object[] data = p.GetObjects();
                        String methodName = (String)data[0];
                        RPCMethodMeta method = _KnownRPCMethods.FirstOrDefault(a => a.MethodName.Equals(methodName));
                        Packet replyPacket = new Packet((UInt16)Packettypes.REPLY);
                        replyPacket.AddObject((Int64)data[1]);
                        replyPacket.AddObject(method?.MethodInfo.Invoke(null, new Object[] { }));
                        source.SendPacket(replyPacket);
                    }
                    break;
                case (UInt16)Packettypes.EXECUTE_RPC_OBJECT:
                    {
                        Object[] data = p.GetObjects();
                        String methodName = (String)data[0];
                        Int64 objectID = (Int64)data[1];
                        IRPCEnabledObject theObject = _RegisteredObjectList[objectID];
                        RPCMethodMeta method = _KnownRPCMethods.FirstOrDefault(a => a.MethodName.Equals(methodName));
                        if (theObject != null) method?.MethodInfo.Invoke(theObject, new Object[] { });
                    }
                    break;
                case (UInt16)Packettypes.EXECUTE_RPC_OBJECT_WITH_REPLY:
                    {
                        Object[] data = p.GetObjects();
                        String methodName = (String)data[0];
                        Int64 objectID = (Int64)data[1];
                        IRPCEnabledObject theObject = _RegisteredObjectList[objectID];
                        RPCMethodMeta method = _KnownRPCMethods.FirstOrDefault(a => a.MethodName.Equals(methodName));
                        if (theObject != null)
                        {
                            Packet replyPacket = new Packet((UInt16)Packettypes.REPLY);
                            replyPacket.AddObject((Int64)data[2]);
                            replyPacket.AddObject(method?.MethodInfo.Invoke(theObject, new Object[] { }));
                            source.SendPacket(replyPacket);
                        }

                    }
                    break;
                case (UInt16)Packettypes.EXECUTE_RPC_STATIC_WITH_ARGS:
                    {
                        Object[] data = p.GetObjects();
                        String methodName = (String)data[0];
                        RPCMethodMeta method = _KnownRPCMethods.FirstOrDefault(a => a.LookupString.Equals(methodName));
                        if (method == null) throw new InvalidDataException($"Packet provided method name {methodName} which could not be found");
                        if (IsServer)
                        {
                            for (Int32 i = 0; i < method.MethodParameters.Count; i++)
                            {
                                if (method.MethodParameters[i].ParameterType == typeof(ClientConnection))
                                {
                                    data[i + 1] = source;
                                }
                            }
                        }

                        method.MethodInfo.Invoke(null, data.Skip(1).ToArray());
                    }
                    break;
                case (UInt16)Packettypes.EXECUTE_RPC_OBJECT_WITH_ARGS:
                    {
                        Object[] data = p.GetObjects();
                        String methodName = (String)data[0];
                        Int64 objectID = (Int64)data[1];
                        IRPCEnabledObject theObject = _RegisteredObjectList[objectID];
                        RPCMethodMeta method = _KnownRPCMethods.FirstOrDefault(a => a.LookupString.Equals(methodName));
                        if (method == null) throw new InvalidDataException($"Packet provided method name {methodName} which could not be found");
                        if (IsServer)
                        {
                            for (Int32 i = 0; i < method.MethodParameters.Count; i++)
                            {
                                if (method.MethodParameters[i].ParameterType == typeof(ClientConnection))
                                {
                                    data[i + 2] = source;
                                }
                            }
                        }

                        method.MethodInfo.Invoke(theObject, data.Skip(2).ToArray());
                    }

                    break;
                case (UInt16)Packettypes.REGISTER_RPC_OBJECT_UPDATE:
                    {
                        Object[] data = p.GetObjects();
                        foreach (Packet objectPacket in data.Cast<Packet>())
                        {
                            Object[] objectPacketData = objectPacket.GetObjects();
                            Int64 objID = (Int64)objectPacketData[0];
                            RPCObjectMeta objType = _KnownRPCTypes.FirstOrDefault(a => a.LookupString.Equals((String)objectPacketData[1]));
                            if (objType == null) throw new InvalidOperationException("Unknown Object Type Specified");
                            if (!_RegisteredObjectList.ContainsKey(objID))
                            {
                                IRPCEnabledObject theObj = (IRPCEnabledObject)Activator.CreateInstance(objType.ObjectType);
                                Int32 objArrayPos = 2;
                                foreach (RPCObjectMeta.RPCObjectPropertyMeta rpcObjectPropertyMeta in objType.PropertyInfo)
                                {
                                    rpcObjectPropertyMeta.PropertyInfo.SetValue(theObj, objectPacketData[objArrayPos++]);
                                }

                                _RegisteredObjectList.Add(theObj.RCPID, theObj);
                                _OnObjectCreated?.Invoke(theObj);
                            }
                            else
                            {
                                IRPCEnabledObject theObj = _RegisteredObjectList[objID];
                                Int32 objArrayPos = 2;
                                foreach (RPCObjectMeta.RPCObjectPropertyMeta rpcObjectPropertyMeta in objType.PropertyInfo)
                                {
                                    rpcObjectPropertyMeta.PropertyInfo.SetValue(theObj, objectPacketData[objArrayPos++]);
                                }
                            }
                        }
                    }
                    break;
            }
        }

        public Object GetRegisteredObject(Int64 id)
        {
            return _RegisteredObjectList.FirstOrDefault(a => a.Key == id).Value;
        }

        public Boolean IsClient => _Client != null;
        public Boolean IsServer => _ServerInstance != null;

        public static RPCEngine CreateAsClient(IPAddress ipAddress, UInt16 port)
        {
            return new RPCEngine { _Client = new BaseClient(), _TargetIPAddress = ipAddress, _TargetPort = port };
        }

        public static RPCEngine CreateAsServer(IPAddress ipAddress, UInt16 port)
        {
            RPCEngine engine = new RPCEngine { _TargetIPAddress = ipAddress, _TargetPort = port };
            engine._ServerInstance = new ServerInstance(engine);
            return engine;
        }

    }
}
