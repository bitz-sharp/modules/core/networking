﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Bitz.Modules.Core.Networking.RPCSystem.Shared
{
    internal class RPCMethodMeta
    {
        internal struct RPCMethodParameterMeta
        {
            internal readonly String ParameterName;
            internal readonly Type ParameterType;

            internal RPCMethodParameterMeta(String name, Type type)
            {
                ParameterName = name;
                ParameterType = type;
            }
        }

        internal readonly String MethodName;
        internal readonly Type MethodType;
        internal readonly MethodInfo MethodInfo;
        private readonly String _LookupString;
        internal IReadOnlyList<RPCMethodParameterMeta> MethodParameters => _MethodParameters.AsReadOnly();

        internal String LookupString => _LookupString;

        private readonly List<RPCMethodParameterMeta> _MethodParameters;

        internal RPCMethodMeta(MethodInfo methodInfo)
        {
            MethodName = methodInfo.Name;
            MethodType = methodInfo.DeclaringType;
            MethodInfo = methodInfo;
            _MethodParameters = new List<RPCMethodParameterMeta>();
            String paramsDesc = "";
            foreach (ParameterInfo parameter in methodInfo.GetParameters())
            {
                _MethodParameters.Add(new RPCMethodParameterMeta(parameter.Name, parameter.ParameterType));
                paramsDesc += $":{parameter.ParameterType}";
            }
            _LookupString = $"{MethodName}{paramsDesc}";
        }

        public override String ToString()
        {
            StringBuilder theParams = new StringBuilder();
            foreach (RPCMethodParameterMeta rpcMethodParamrterMeta in _MethodParameters)
            {
                theParams.Append($"{rpcMethodParamrterMeta.ParameterName}/{rpcMethodParamrterMeta.ParameterType},");
            }
            return $"{MethodName} from {MethodType} with paramters {theParams}";
        }
    }
}
