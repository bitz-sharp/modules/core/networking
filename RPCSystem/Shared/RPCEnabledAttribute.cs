﻿using System;

namespace Bitz.Modules.Core.Networking.RPCSystem.Shared
{
    [AttributeUsage(AttributeTargets.Method|AttributeTargets.Property)]
    public class RPCEnabled : Attribute
    {
    }
}
