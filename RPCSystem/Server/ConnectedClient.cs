﻿using System;
using System.Net.Sockets;
using Sbatman.Networking.Server;
using Sbatman.Serialize;

namespace Bitz.Modules.Core.Networking.RPCSystem.Server
{
    public class ConnectedClient : ClientConnection
    {
        public static Object _LockingObject = new Object();
        public ConnectedClient(BaseServer server, TcpClient newSocket) : base(server, newSocket)
        {
            _Server = server;
        }

        protected override void OnConnect()
        {

        }

        protected override void OnDisconnect()
        {
        }

        protected override void ClientUpdateLogic()
        {
            foreach (Packet packet in GetOutStandingProcessingPackets())
            {
                (_Server as ServerInstance).Engine.ProcessPacket(packet, this);
            }
        }

        protected override void HandelException(Exception e)
        {

        }
    }
}
