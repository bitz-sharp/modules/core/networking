﻿using Bitz.Modules.Core.Networking.RPCSystem.Shared;

namespace Bitz.Modules.Core.Networking.RPCSystem.Server
{
    class ServerInstance : Sbatman.Networking.Server.BaseServer
    {
        public readonly RPCEngine Engine;
        public ServerInstance(RPCEngine engine)
        {
            Engine = engine;
        }
    }
}
